// Importamos el paquete express
import express, { request } from 'express';


import * as productos from './productos';


// Instanciamos una app de express
const app = express();
//Definir puerto
const puerto: Number = 3000;
//Definir URI.
const uri: String = 'http://localhost:3000';
//Ejecutar Express.
app.use(express.json());

app.get('/productos',function(request, response) {
    response.send( productos.getStock()  );
});


app.post('/productos', function(request, response) {
    const body = request.body
    response.send(productos.storeProductos(body))
    response.send("Agregaremos un producto a la lista");
});


app.delete('/productos/:id' , function(request, response) {
    const idProducto: any = request.params.id
    response.send(productos.deleteProductos(idProducto));
    response.send("Eliminaremos un producto de la lista");
});



// Definimos una ruta y su handler correspondiente
app.get('/', function (request, response) {
    response.send('¡Bienvenidos a Express!');
});



// Ponemos a escuchar nuestra app de express
app.listen(puerto, function () {
    console.info( `Servidor escuchando en ${uri}`);
});
